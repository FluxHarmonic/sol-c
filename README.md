# sol-c

## Building the Code

Make sure you have the GCC toolchain and basic tools installed (gcc, make, coreutils, awk, sed, find) and then run the following commands:

```sh
git clone --recursive https://codeberg.org/FluxHarmonic/sol-c
./configure
make
```

You can also run the program by executing `make run`.

### Building for the web

The `./configure` script should automatically detect the `EMSDK` variable in the Emscripten SDK shell and configure the project correctly.  If it doesn't, you can use the `--plat=wasm` parameter to switch to `emcc` et al.

```sh
./configure --plat=wasm
make
```

To build with Emscripten without installing `emsdk` you can use Docker:

```sh
docker run -v `pwd`:`pwd` -w `pwd` -u $(id -u):$(id -g) -p 8080:6931 emscripten/emsdk /bin/sh -c "./configure && make"
```

To test the Emscripten build locally, use `emrun`:

```sh
docker run -v `pwd`:`pwd` -w `pwd` -u $(id -u):$(id -g) -p 8080:6931 emscripten/emsdk /bin/sh -c "emrun ./build/wasm/wasm32/release --no_browser"
```
