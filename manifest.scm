(specifications->manifest
 '("coreutils"
   "gcc-toolchain"
   "pkg-config"
   "make"
   "findutils"
   "gawk"
   "sed"

   "glfw"

   ;; Command line tools
   "git"
   "tar"
   "zip"
   "wget"
   "gzip"
   "openssh"
   "nss-certs"

   ;; Tools needed for emsdk to operate
   "gcc:lib"
   "zlib"
   "python"
   "python-certifi"
   "which"
   "tar"
   "xz"
   "lbzip2"))
