#!/bin/sh

set_project "sol-c"
set_version "0.0.1" "%Y%m%d%H%M"

includes "lib/flux-lib"

option "debug" "Enable debug build." false
option "tests" "Enable tests." true

set_warnings "all" "error"
set_languages "gnu99"

if is_mode "debug"; then
    set_symbols "debug"
    set_optimizes "none"
    add_defines "DEBUG"
else
    set_strip "all"
    set_symbols "hidden"
    set_optimizes "smallest"
fi

target "build"
    set_kind "binary"
    set_filename "sol-c"
    add_files "src/*.c"
    add_deps "flux"
    add_flux_lib_flags
    if is_toolchain "emcc"; then
        set_filename "index.html"
        add_ldflags "--preload-file assets"
    else
        add_installfiles "assets/" "share"
    fi
