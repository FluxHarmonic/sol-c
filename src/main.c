// clang-format off
#include "sokol/sokol_gfx.h"
#include "sokol/sokol_gl.h"
#include "sokol/sokol_debugtext.h"
#include "sokol/sokol_log.h"
#include "sokol/sokol_time.h"
#include "fontstash/fontstash.h"
#include "sokol/sokol_fontstash.h"
#include "stb/stb_image.h"
#include "flux-ui.h"
// clang-format on

#define GLFW_INCLUDE_NONE
#include "GLFW/glfw3.h"

#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#endif

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

#define CARD_WIDTH 50 * 2
#define CARD_HEIGHT 80 * 2
#define CARD_H_SPACING 20

// Characters: ♥ ♦ ♠ ♣
typedef enum { SuitNone, SuitHeart, SuitDiamond, SuitSpade, SuitClub } CardSuit;

typedef uint8_t CardRank;

typedef struct {
  CardRank rank;
  CardSuit suit;
} Card;

typedef struct {
  Card card;
  bool is_hidden;
} StackCard;

typedef enum { StateNone, StateDown, StateUp } StackCardState;

typedef struct {
  uint8_t num_cards;
  StackCard cards[13];
} Stack;

typedef struct {
  CardSuit suit;
  CardRank rank;
} ClearedCards;

#define FONT_BUFFER_SIZE 256 * 1024

typedef struct {
  uint8_t pool_count, pool_index;
  Card pool_cards[24];
  ClearedCards cleared[4];
  Stack stacks[7];

  GLFWwindow *window;
  sgl_pipeline pip;
  float start_time;
  uint64_t frame_time;
  sg_image card_image;
  /* FONScontext *fons; */
  int font_main;
  uint8_t font_main_data[FONT_BUFFER_SIZE];
} GameState;

GameState new_state() {
  GameState state = {.pool_count = 24,
                     .pool_index = 0,
                     .cleared = {[0] = {.suit = SuitNone, .rank = 0},
                                 [1] = {.suit = SuitNone, .rank = 0},
                                 [2] = {.suit = SuitNone, .rank = 0},
                                 [3] = {.suit = SuitNone, .rank = 0}},
                     .stacks =
                         {
                             [0] = {.num_cards = 1, .cards = {0}},
                             [1] = {.num_cards = 2, .cards = {0}},
                             [2] = {.num_cards = 3, .cards = {0}},
                             [3] = {.num_cards = 4, .cards = {0}},
                             [4] = {.num_cards = 5, .cards = {0}},
                             [5] = {.num_cards = 6, .cards = {0}},
                             [6] = {.num_cards = 7, .cards = {0}},
                         },
                     .frame_time = 0,
                     .start_time = 0,
                     .card_image = {0},
                     .window = NULL};

  return state;
}

static sg_image load_image(const char *filename) {
  int width, height, channels;
  sg_image img = {SG_INVALID_ID};

  uint8_t *data = stbi_load(filename, &width, &height, &channels, 4);
  if (!data) {
    printf("IMAGE FAILED TO LOAD!\n");
    return img;
  }

  sg_image_desc image_desc;
  memset(&image_desc, 0, sizeof(sg_image_desc));
  image_desc.width = width;
  image_desc.height = height;
  image_desc.pixel_format = SG_PIXELFORMAT_RGBA8;
  image_desc.min_filter = SG_FILTER_NEAREST;
  image_desc.mag_filter = SG_FILTER_NEAREST;
  image_desc.wrap_u = SG_WRAP_REPEAT;
  image_desc.wrap_v = SG_WRAP_REPEAT;
  image_desc.data.subimage[0][0].ptr = data;
  image_desc.data.subimage[0][0].size = (size_t)(width * height * 4);

  img = sg_make_image(&image_desc);
  stbi_image_free(data);

  return img;
}

void game_loop(GameState *game_state) {
  int current_width, current_height;
  glfwGetFramebufferSize(game_state->window, &current_width, &current_height);

  float ratio = current_width / (float)current_height;
  sgl_defaults();
  sgl_matrix_mode_projection();
  sgl_ortho(0.0f, current_width, current_height, 0.0f, -1.f, 1.f);
  sgl_load_pipeline(game_state->pip);
  sgl_enable_texture();

  double this_frame_time = stm_sec(
      stm_round_to_common_refresh_rate(stm_laptime(&game_state->frame_time)));
  sdtx_canvas(current_width, current_height);
  sdtx_origin(1, 1);
  sdtx_printf("FPS: %.2f", 1.0f / this_frame_time);

  float scale =
      fmin(2.0f, fmax(1.0f, fmin(current_width * 1.f / SCREEN_WIDTH,
                                 current_height * 1.f / SCREEN_HEIGHT)));
  float board_width = 7 * CARD_WIDTH + CARD_H_SPACING * 6;
  sgl_scale(scale, scale, 1.f);
  sgl_translate(-board_width / 2, 0.f, 0.f);
  sgl_translate(current_width / 2 / scale, 0.f,
                0.f); // Divide by scale to cancel out the scale projection

  // Activate the card texture
  sgl_texture(game_state->card_image);

  // Pool cards
  for (int i = 0; i < 7; i++) {
    if (i != 2) {
      float x = (CARD_WIDTH + CARD_H_SPACING) * i, y = 50;
      sgl_begin_quads();
      /* sgl_c4f(1.f, 1.f, 1.f, 1.f); */
      sgl_v2f_t2f(x, y, 0.0f, 0.0f);
      sgl_v2f_t2f(x + CARD_WIDTH, y, 1.0f, 0.0f);
      sgl_v2f_t2f(x + CARD_WIDTH, y + CARD_HEIGHT, 1.0f, 1.0f);
      sgl_v2f_t2f(x, y + CARD_HEIGHT, 0.0f, 1.0f);
      sgl_end();
    }
  }

  // Stack cards
  for (int i = 0; i < 7; i++) {
    for (int j = 0; j < game_state->stacks[i].num_cards; j++) {
      // TODO: Adjust vertical spacing based on stack card count.  The key is to
      // ensure that the bottom of the bottom-most card is visible.
      float x = (CARD_WIDTH + CARD_H_SPACING) * i, y = 250 + 30 * j;
      sgl_begin_quads();
      /* sgl_c4f(1.f, 1.f, 1.f, 1.f); */
      sgl_v2f_t2f(x, y, 0.0f, 0.0f);
      sgl_v2f_t2f(x + CARD_WIDTH, y, 1.0f, 0.0f);
      sgl_v2f_t2f(x + CARD_WIDTH, y + CARD_HEIGHT, 1.0f, 1.0f);
      sgl_v2f_t2f(x, y + CARD_HEIGHT, 0.0f, 1.0f);
      sgl_end();
    }
  }

  // Disable the card texture
  sgl_disable_texture();

  // Draw some text
  /* FONScontext *fs = game_state->fons; */
  /* fonsSetFont(fs, game_state->font_main); */
  /* fonsSetSize(fs, 20.0f); */
  /* /\* fonsSetColor(fs, sfons_rgba(172, 50, 50, 255)); *\/ */
  /* fonsSetColor(fs, sfons_rgba(0, 0, 0, 255)); */
  /* fonsDrawText(fs, 100 + 2, 240 + 3, "♥♦♠♣A123456789JQK", NULL); */
  /* fonsSetColor(fs, sfons_rgba(255, 255, 255, 255)); */
  /* fonsDrawText(fs, 100, 240, "♥♦♠♣A123456789JQK", NULL); */

  // Flush text rendering
  /* sfons_flush(fs); */

  // Draw animation
  /* float time = stm_sec(stm_since(game_state->start_time)); */
  /* const float speed = 0.7 + (sinf(time) * 0.1f); */
  /* const int count = 6; */
  /* const float scale = 1.f - (sinf(time * speed) * 0.4f); */
  /* sgp_scale_at(scale, scale, current_width / 2.f, current_height / 2.f); */
  /* sgp_set_blend_mode(SGP_BLENDMODE_BLEND); */
  /* for (int i = 0; i < count; i++) { */
  /*   float this = (time * speed) + (i * (2 * M_PI) / count); */
  /*   float x = sinf(this) * 300 + (current_width / 2), */
  /*         y = cosf(this) * 300 + (current_height / 2); */
  /*   sgp_push_transform(); */
  /*   sgp_set_image(0, game_state->card_image); */
  /*   sgp_reset_color(); */
  /*   sgp_translate(x - CARD_WIDTH / 2, y - CARD_HEIGHT / 2); */
  /*   sgp_rotate_at(time / 0.5f, CARD_WIDTH / 2, CARD_HEIGHT / 2); */
  /*   sgp_draw_textured_rect(0, 0, CARD_WIDTH, CARD_HEIGHT); */
  /*   sgp_pop_transform(); */
  /* } */

  simgui_new_frame(&(simgui_frame_desc_t){.width = current_width,
                                          .height = current_height,
                                          .delta_time = this_frame_time,
                                          .dpi_scale = 1.f});

  igText("Hello, world!");

  // Perform the render pass
  sg_pass_action pass_action = {
      .colors[0] = {.action = SG_ACTION_CLEAR,
                    .value = {0.153f, 0.278f, 0.129f, 1.0f}}};
  sg_begin_default_pass(&pass_action, current_width, current_height);
  sgl_draw();
  sdtx_draw();
  simgui_render();
  sg_end_pass();
  sg_commit();

  glfwSwapBuffers(game_state->window);
  glfwPollEvents();
}

int main(int argc, char **argv) {
  glfwInit();
  glfwWindowHint(GLFW_SAMPLES, 0);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow *window =
      glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Sol-C", 0, 0);
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);

  // Time library init
  stm_setup();

  // Sokol init
  sg_setup(&(sg_desc){
      .logger.func = slog_func,
  });
  sgl_setup(&(sgl_desc_t){
      .logger.func = slog_func,
  });

  // Debug text renderer init
  sdtx_setup(&(sdtx_desc_t){.logger.func = slog_func,
                            .fonts = {
                                [0] = sdtx_font_kc854(),
                            }});

  flux_ui_setup(window);

  GameState game_state = new_state();
  game_state.window = window;
  game_state.start_time = stm_now();
  game_state.card_image = load_image("assets/card.png");

  // Initialize the font atlas
  const int atlas_size = 512.0f;
  /* FONScontext *fons_context = sfons_create(&(sfons_desc_t){ */
  /*     .width = atlas_size, */
  /*     .height = atlas_size, */
  /* }); */
  /* game_state.fons = fons_context; */

  // Load fonts
  FILE *fp = fopen("assets/QuinqueFive.ttf", "r");
  size_t size =
      fread(&game_state.font_main_data, sizeof(uint8_t), FONT_BUFFER_SIZE, fp);
  /* game_state.font_main = */
  /*     fonsAddFontMem(game_state.fons, "public", */
  /*                    (void *)&game_state.font_main_data, size, false); */

  // Create a pipeline with the proper blendmode
  game_state.pip = sgl_make_pipeline(&(sg_pipeline_desc){
      .colors[0].blend = {.enabled = true,
                          .src_factor_rgb = SG_BLENDFACTOR_SRC_ALPHA,
                          .dst_factor_rgb =
                              SG_BLENDFACTOR_ONE_MINUS_SRC_ALPHA}});

#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop_arg((em_arg_callback_func)game_loop, &game_state, 0,
                               1);
#else
  while (!glfwWindowShouldClose(window)) {
    game_loop(&game_state);
  }
#endif

  sdtx_shutdown();
  /* sfons_destroy(game_state.fons); */
  sgl_shutdown();
  sg_shutdown();
  glfwTerminate();

  printf("Exiting...\n");
}
